const serve = require('serve');

//just example flags, run serve help from command line to see all flags
const server = serve(__dirname + '/build', {
    port: 1337,
    ignore: ['node_modules']
  });

//to stop call this command:
//server.stop()  