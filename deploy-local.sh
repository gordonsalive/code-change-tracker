echo 'Will build and deploy app locally using serve static server'
echo 'make sure looping scripts are up and running before starting website: ./startup.sh'
# TODO: just use my swagger express server for this
echo 'fetch serve'
# npm install -g serve

echo 'build react app and deploy locally in build/'
npm run build

echo 'serve up the newly built and deployed app'
# serve -s build
# forever stop ./startapp.js
# forever start ./startapp.js
forever start serve-app.js