//
//This set of tests is acting as an exemplar of how to write tests, for reference elsewhere
//

import React from 'react';
//import { expect } from 'chai';
import { shallow } from 'enzyme';

import Enzyme from 'enzyme';//I think these can go into a setup script
import Adapter from 'enzyme-adapter-react-16';//I think these can go into a setup script
Enzyme.configure({ adapter: new Adapter() });//I think these can go into a setup script

//import sinon from 'sinon';
var should = require('chai').should()

import Banner from './Banner';
import FilterForm from './FilterForm';

function add(a, b) {
    return a + b;
}

//Using snap-shot----------
const snapshot = require('snap-shot');
describe('example of using snap-shot', () => {
    it('works', () => {
        snapshot(add(10, 20), 'add');
        snapshot('a text message', 'a text message');
        //return Promise.resolve(42).then(snapshot);
    });
});
//----------------

//Using snap-shot-it----------
// const snapshot = require('snap-shot-it');
// describe('example of using snap-shot-it', () => {
//   it('works', () => {
//     snapshot(add(10, 20));
//     snapshot('a text message');
//     return Promise.resolve(42).then(snapshot);
//   });
// });
//----------------

//Using chai-jest-snapshot----------
// import chai from "chai";
// import chaiJestSnapshot from "chai-jest-snapshot";

// chai.use(chaiJestSnapshot);

// before(function() {
//   chaiJestSnapshot.resetSnapshotRegistry();
// });

// beforeEach(function() {
//   chaiJestSnapshot.configureUsingMochaContext(this);
// });

// import React from "react";
// import renderer from "react-test-renderer";
// import { expect } from "chai";
// import Link from "./Link";

// describe("Link", function() {
//   it("renders correctly", () => {
//     const tree = renderer.create(
//       <Link page="http://www.facebook.com">Facebook</Link>
//     ).toJSON();
//     expect(tree).to.matchSnapshot();
//   });
// });
//----------------

describe('<Banner /> shallow', () => {
    it('renders one <FilterForm /> component', () => {
        const wrapper = shallow(<Banner />);
        //expect(wrapper.find(Foo)).to.have.length(3);
        wrapper.find(FilterForm).should.have.length(1);
    });

    it('renders a `.Banner`', () => {
        const wrapper = shallow(<Banner />);
        //expect(wrapper.find('.Banner')).to.have.length(1);
        wrapper.find('.Banner').should.have.length(1);
    });

    it('renders children when passed in (not!)', () => {
        const wrapper = shallow((
            <Banner>
                <div className="unique" />
            </Banner>
        ));
        console.log(wrapper.text());
        //expect(wrapper.contains(<div className="unique" />)).to.equal(true);
        wrapper.contains(<div className="unique" />).should.equal(false);
        //TODO: change Banner so that it does render children!
    });

    it('simulates click events (not!)', () => {
        const onButtonClick = sinon.spy();
        const wrapper = shallow(<FilterForm onButtonClick={onButtonClick} />);
        wrapper.find('button').simulate('click');
        //expect(onButtonClick).to.have.property('callCount', 1);
        onButtonClick.should.have.property('callCount', 0);
        //TODO: change FilterForm so that I can set the onButtonClick method to my spy
    });

    it('has`t changed form the snapshot (shallow)', () => {
        const wrapper = shallow(<Banner />);
        snapshot(wrapper.text(), 'has`t changed form the snapshot (shallow)');//using snap-shot
        //snapshot(wrapper.text());//using snap-shot-it
        //wrapper.text().should.matchSnapshot();//using chai-jest-snapshot
    })
});

import { mount } from 'enzyme';

describe('<Banner /> deep', () => {
    it('allows us to set props', () => {
        const wrapper = mount(<Banner bar="baz" />);
        wrapper.props().bar.should.equal('baz');
        wrapper.setProps({ bar: 'foo' });
        wrapper.props().bar.should.equal('foo');
    });

    it('simulates click events (not)', () => {
        const onButtonClick = sinon.spy();
        const wrapper = mount((
            <Banner onButtonClick={onButtonClick} />
        ));
        //TODO: change Banner so that I can set the onButtonClick method to my spy
        wrapper.find('button').simulate('click');
        onButtonClick.should.have.property('callCount', 0);
    });

    it('has`t changed form the snapshot (deep)', () => {
        const wrapper = mount(<Banner />);
        snapshot(wrapper.text(), 'has`t changed form the snapshot (deep)');//using snap-shot
        //snapshot(wrapper.text());//using snap-shot-it
        //wrapper.text().should.matchSnapshot();//using chai-jest-snapshot
    });

    //   it('calls componentDidMount', () => {
    //     sinon.spy(Banner.prototype, 'componentDidMount');
    //     const wrapper = mount(<Banner />);
    //     Banner.prototype.componentDidMount.should.have.property('callCount', 1);
    //     Banner.prototype.componentDidMount.restore();
    //   });
});