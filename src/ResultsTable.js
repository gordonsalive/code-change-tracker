import React, { Component } from 'react';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import './css/ResultsTable.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
//const Q = require('q');

export class ResultsTable extends Component {
    //PROPS:
    // results: lifted up from here to the Component above, so that component is in charge of fetching its own data
    // columns: column attributes that are consumed by BootstrapTable
    // fetchhHandler: method to fetch and return data when Fetch is clicked
    //                --or supply raw data to display immediately (and don't show Fetch button)--not  raw data anymore, after lifting results.
    // clearFilterHandler: method to clear the filter, 
    //                     will show Clear Filter button if method supplied
    // isSmall: decides if table contents and headers should be rendered in a smaller text
    // fetchImmediate: Sometimes we want a Fetch button to allow refresh, 
    //                 be also want to fetch straight away
    // isLoading: whether to show the table as loading/busy
    constructor(props) {
        super(props);
        this.state = {
            //results: [],
            showFetch: false,
            showClearFilter: (props.clearFilterHandler ? true : false)
        };
        if (props.fetchHandler) {
            //if fetchHandler is a method then need to show Fetch Button
            if (typeof props.fetchHandler === "function") {
                this.state.showFetch = true;
            }
            // //if fetchHandler is not a method or fetchImmediate, then set results now
            // if (props.fetchImmediate || (typeof props.fetchHandler !== "function")) {
            //     if (typeof props.fetchHandler === "function") {
            //         this.state.results = props.fetchHandler();
            //     } else {
            //         this.state.results = props.fetchHandler;
            //     }
            // }

            // if I've been told to fetch immediate, and I've been given a method, then fetch it!
            if (props.fetchImmediate && (typeof props.fetchHandler === "function")) {
                props.fetchHandler();
            }
        }
    }
    handleFetchClick() {
        //now results has been lifted up, just call the fetchHandler and let that set the results
        this.props.fetchHandler();

        // //this seems to go out of scope when inside promise, so capture it here
        // var resultsTable = this;
        // if (this.props.fetchHandler) {
        //     //data may come back as a promise, so need to handle this
        //     Q(this.props.fetchHandler())
        //         .then(function(results) {
        //             resultsTable.setState({
        //                 results: results
        //             });
        //         })
        // }
    }

    handleClearFilterClick() {
        if (this.props.clearFilterHandler) {
            this.props.clearFilterHandler(this.refs);
        }
    }
    tableHeaderColumns() {
        if (this.props.columns) {
            const renderColumns = this.props.columns.map((origCol) => {
                var col = JSON.parse(JSON.stringify(origCol));//copy the col so we can extend it (note will convert date to date string)
                if (col.filter) {
                    if (col.filter.thStyle) {
                        col.filter.thStyle.whiteSpace = 'normal';
                    } else {
                        col.filter.thStyle = { whiteSpace: 'normal' };
                    }
                    if (this.props.isSmall) {
                        if (col.filter.style) {
                            col.filter.style.fontSize = "xx-small";
                        } else {
                            col.filter.style = { fontStyle: "xx-small" };
                        }
                    }
                    if (!col.filter.delay) {
                        col.filter.delay = 1000;
                    }
                    if (col.isDate) {
                        if (!col.filter.type) {
                            col.filter.type = 'DateFilter';
                        }
                        if (!col.filter.defaultValue) {
                            col.filter.defaultValue = { date: new Date(), comparator: '>=' }
                        } else {
                            col.filter.defaultValue.date = new Date(col.filter.defaultValue.date);//correct for date being turned into date string in copy above
                        }
                        if (this.props.isSmall) {
                            if (!col.filter.style.date) {
                                col.filter.style.date = { fontSize: "xx-small" };
                            } else {
                                col.filter.style.date.fontSize = "xx-small";
                            }
                            if (!col.filter.style.comparator) {
                                col.filter.comparator = { fontSize: "xx-small" };
                            } else {
                                col.filter.comparator.fontSize = "xx-small";
                            }
                        }
                    } else {
                        if (!col.filter.type) {
                            col.filter.type = 'TextFilter';
                        }
                    }
                }
                const keyRef = col.ref ? col.ref : (col.dataField ? col.dataField : col.name);
                return (
                    <TableHeaderColumn
                        key={keyRef}
                        isKey={col.isKey}
                        ref={keyRef}
                        dataField={col.dataField ? col.dataField : col.name}
                        width={col.width}
                        dataSort={true}
                        dataFormat={col.isDate ? dateFormatter : (col.isCheckbox ? checkboxFormatter : undefined)}
                        filter={col.filter}>
                        {col.name}
                    </TableHeaderColumn>
                )
            });
            return renderColumns;
        }
    }
    renderFetchButton() {
        if (this.state.showFetch) {
            return (
                <button className="badge badge-pill badge-success"
                    onClick={() => this.handleFetchClick()}>
                    Fetch
                </button>
            )
        }
    }
    renderClearFilterButton() {
        if (this.state.showClearFilter) {
            return (
                <a className="badge badge-pill badge-success"
                    onClick={this.handleClearFilterClick.bind(this)}
                    style={{ cursor: 'pointer' }}>
                    Clear Filters
                </a>
            )
        }
    }
    renderLoading() {
        if (this.props.isLoading) {
            return (
                <div className="wow flash box" data-wow-iteration="100">Loading...</div>
            )
        }
    }
    render() {
        console.log('this.props.isLoading=' + this.props.isLoading);
        return (
            <div className="ResultsTable" style={ this.props.isLoading ? {cursor: 'progress'} : {cursor: 'auto'} }>
                {this.renderFetchButton()}
                {this.renderClearFilterButton()}
                {this.renderLoading()}
                <span className={this.props.isSmall ? "small input-sm" : ""}>
                    {/* <small> */}
                    <BootstrapTable 
                        //data={this.state.results} 
                        data={this.props.results} 
                        striped hover condensed options={{ noDataText: 'No data' }}>
                        {this.tableHeaderColumns()}
                    </BootstrapTable>
                    {/* </small> */}
                </span>
            </div>
        );
    }
}

function checkboxFormatter(cell, row) {
    return `<input type='checkbox' ${cell ? 'checked' : ''}>`;
}

function dateFormatter(cellDateString, row) {
    let cell = new Date(cellDateString);
    let day = ('0' + cell.getDate()).slice(-2);
    let month = ('0' + (cell.getMonth() + 1)).slice(-2);
    let fullYear = cell.getFullYear();
    //console.log(`${day}/${month}/${fullYear}`);
    return `${day}/${month}/${fullYear}`;
}

export default ResultsTable;
