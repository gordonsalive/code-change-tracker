import React, { Component } from 'react';
import './css/MainPage.css';
import {Banner} from './Banner';
import {CodeChangesResultsTable} from './CodeChangesResultsTable';

export class MainPage extends Component {
	codeChangesResultsTable = null;
	// sorts out the layout of the main page, banner at the top, results table underneath
	constructor(props) {
		super(props);
		this.state = {
			filterState: {}
		};

        //(note, these binds aren't actually needed if methods don't use 'this' or are called with => notation)
		this.handleSubmitClick = this.handleSubmitClick.bind(this);
		this.handleCodeChangesResultsTableCreated = this.handleCodeChangesResultsTableCreated.bind(this);
	}
	handleSubmitClick(newFilterState) {
		console.log('handling submit click, filter state');// = '+ JSON.stringify(newFilterState) );
		//update my filter state
		this.setState({
			filterState: newFilterState
		});
		//this is a hack, I'm trying to avoid lifting results all the way up here, but perhaps I should
		//have a TableWithFilterForm class and lift results up to that level.
		const mainPage = this;
		setTimeout(function(){
			console.log('waited for state change');//'MainPage.state=' + JSON.stringify(mainPage.state));
			mainPage.codeChangesResultsTable.getData()
		}, 300);//after 300ms kick off the data fetch with this new filter in place, right?
	}
	handleCodeChangesResultsTableCreated(table) {
		//console.log('setting reference to changes table: ' + table);
		this.codeChangesResultsTable = table;
	}
	render() {
		return (
			<div className="MainPage">
				<div className="wow fadeInDown box">
					<header className="App-header">
						{/* <img src={logo} className="App-logo" alt="logo"/> */}
						<i className="far fa-file-code fa-4x"></i>
						<h1 className="App-title">Welcome to the code change tracker</h1>
					</header>
				</div>
				<div className="wow fadeIn box">
					<p className="App-intro">
						Below you can see the config used to collect this list of changes and you can pre-filter the data to make the table faster.
						<br />
						You can further filter and sort the data within the table.
					</p>
					<div className="container-fluid">
						<Banner onSubmitClick={this.handleSubmitClick}/>
						<CodeChangesResultsTable 
							filterState={this.state.filterState}
							onCreated={this.handleCodeChangesResultsTableCreated}
						 	//testData={true}
						 	/>
					</div>
				</div>
			</div>
		);
	};
};

export default MainPage;
