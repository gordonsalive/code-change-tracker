import React from 'react';
import { shallow, mount } from 'enzyme';
const snapshot = require('snap-shot');
var should = require('chai').should()
//import sinon from 'sinon';
import CodeChangesResultsTable from './CodeChangesResultsTable';

import Enzyme from 'enzyme';//I think these can go into a setup script
import Adapter from 'enzyme-adapter-react-16';//I think these can go into a setup script
Enzyme.configure({ adapter: new Adapter() });//I think these can go into a setup script

describe('<CodeChangesResultsTable /> shallow', () => {
    it('renders a `.CodeChangesResultsTable`', () => {
        const wrapper = shallow(<CodeChangesResultsTable testData={true}/>);
        wrapper.find('.CodeChangesResultsTable').should.have.length(1);
    });

    it('has`t changed form the snapshot (shallow)', () => {
        const wrapper = shallow(<CodeChangesResultsTable  testData={true}/>);
        snapshot(wrapper.text(), 'has`t changed form the snapshot (shallow)');//using snap-shot
    })
});

describe('<CodeChangesResultsTable /> deep', () => {
    it('allows us to set props', () => {
        const wrapper = mount(<CodeChangesResultsTable testData={true} bar="baz" />);
        wrapper.props().bar.should.equal('baz');
        wrapper.setProps({ bar: 'foo' });
        wrapper.props().bar.should.equal('foo');
    });

    it('has`t changed form the snapshot (deep)', () => {
        const wrapper = mount(<CodeChangesResultsTable  testData={true}/>);
        snapshot(wrapper.text(), 'has`t changed form the snapshot (deep)');//using snap-shot
    });
});