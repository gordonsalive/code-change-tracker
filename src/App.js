import React, { Component } from 'react';
import './css/App.css';
import {MainPage} from './MainPage';

class App extends Component {
	render() {
		return (
			<div className="App" id="bootstrap-overrides">
				<MainPage />
			</div>
		);
	}
}

export default App;
