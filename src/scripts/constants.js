// AAG 23/1/'18
// This script just holds constants so they can be changed in a single place
module.exports = {
    // Config files
    repos: (depth) => { return depthDots(depth) + 'config/repos.json' },
    codeChangesStatus: (depth) => { return depthDots(depth) + 'logs/code-changes-status.json' },
    codeChangesResults: (depth) => { return depthDots(depth) + 'data/code-changes-results.json' },
    codeChangesResultsXml: (depth) => { return depthDots(depth) + 'data/code-changes-results.xml' },
    codeChangesResultsFlat: (depth) => { return depthDots(depth) + 'data/code-changes-results-flat.json' },
    codeChangesResultsFlatXml: (depth) => { return depthDots(depth) + 'data/code-changes-results-flat.xml' },
    publishFilesStatus: (depth) => { return depthDots(depth) + 'logs/publish-files-status.json' }
}

module.exports.filesToPublish = [
    module.exports.repos(0),
    module.exports.codeChangesStatus(0),
    module.exports.codeChangesResults(0),
    module.exports.codeChangesResultsXml(0),
    module.exports.codeChangesResultsFlat(0),
    module.exports.codeChangesResultsFlatXml(0),
    module.exports.publishFilesStatus(0)
]

let dataServer = '172.26.27.188:10010';
//let dataServer = 'localhost:10010'//use this is you want to use the local swagger server and local data

module.exports.dataSources = {
    repos: './scripts/config/repos.json',//doesn't change, just require() it
    codeChangesResultsFlat: 'http://' + dataServer + '/code-changes-results-flat'//constantly changes get via swagger server
}

function depthDots(depth) {
    let _ = require("underscore");

    if (depth) {
        let dots = _.range(depth).map(function (depth) { return '..' });
        console.log(dots);
        console.log(dots.join('/') + '/');
        return dots.join('/') + '/';
    } else {
        return './';
    }
}