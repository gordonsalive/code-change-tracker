const file_utils = require('./utils/file-utils');
const consts = require('./constants');
const Q = require('q');

console.log('================================================================================');
console.log('Start: ' + new Date().toISOString());
console.log('================================================================================');

// AAG 23/1/'18
// This script calls other scripts to:
// (1) fetch all the SVN code changes for the last 3 months, grouped by job, jira or incident no
// (2) repeat every 15 minutes between 8am and 6pm

const DEBUG = 1; //0 = off

writeOutStatus('started');

(function doMainStuff() {
    Q(true)
        .then(function(result) {
            console.log(" " + new Date() + ": starting to copy files.");
            return consts.filesToPublish.map(item => {
                const filename = item.split('/').reverse()[0];
                console.log(item + ':' + filename);
                return file_utils.copyFile(item, '../../public/data/' + filename, true) &&
                file_utils.copyFile(item, '../../build/data/' + filename, true) &&
                file_utils.copyFile(item, '../../server/data/' + filename, true);
            });
        })
        .then(function (results) {
            //console.log('about to write out success');
            writeOutStatus('succeeded');

            let d = new Date();
            if (DEBUG) console.log('Completed: ' + d.toLocaleString() + '. Wait 1 minute before repeating...');

            setTimeout(doMainStuff, 1.5 * 60 * 1000); //try again in 1.5 minutes
        })
        .fail(function (err) {
            console.error(err);
            
            writeOutStatus('failed', err);

            let d = new Date();
            console.log(d.toLocaleString() + ': error, try again in 15 minutes');

            setTimeout(doMainStuff, 15 * 60 * 1000); //try again in 15 minutes
        });
})();  

function writeOutStatus(connectionStatus, error) {
    let update_status = require('./utils/update-status');

    update_status(connectionStatus, 'publish-files', error);
}