//Also, want to add a chain method that takes an array of promises and runs then one after the other
//like this:
//var funcs = [foo, bar, baz, qux];

// var result = Q(initialVal);
// funcs.forEach(function (f) {
//     result = result.then(f);
// });
// return result;