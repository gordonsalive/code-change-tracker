// AAG 23/1/'18
// This script exposes methods to turn data & methods into promises

module.exports = function (obj) {
    //takes a bit of data and wraps it up in a promise
    let Q = require("q");

    return Q(obj);
};

module.exports.method = function (func) {
    //takes a method and wraps it up in a promise
    let Q = require("q");

    return Q.fcall(func);
};

module.exports.spread = function (promiseArray, func) {
    //takes an array of promises and runs func against them, collecting the result as an array of promises
    let Q = require("q");

    return Q.spread(promiseArray, func);
};