//import { promisify } from "utils";
//import { lock } from "lockfile";

// AAG 23/1/'18
// This script parses a json file and returns the json object.
// (returns a promise)

module.exports = function (filename) {
    //let Qfs = require("q-io/fs");
    //let Q = require("q");

    let file_utils = require("./file-utils");

    let DEBUG = 1; //0=off

    if (DEBUG) console.log('read-json from file:' + filename);
    
    //when file is read, then, parse the json and return as a promise so it can be chained.
    return file_utils.readFile(filename)
        .then(function (data) {
            console.log('about to return parsed JSON');
            return JSON.parse(data);
        });
};