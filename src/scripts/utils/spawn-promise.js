// AAG 24/1/'18
// This script spawns a task on the command line and captures the output.
// ('spawn' is different from 'execute')
// (returns a promise)

module.exports = function (command, silent, timeout) {
    let DEBUG = 0; //0=off

    let Qfs = require("q-io/fs");
    let Q = require("q");
    let fs = require('fs');

    let commandAndArgs = command.split(' ');
    //if (DEBUG) console.log(commandAndArgs);
    return Q.Promise(function (resolve, reject, notify) {
        let noisy = !silent;
        let child_process = require('child_process');
        let oldSpawn = child_process.spawn;

        function mySpawn() {
            if (DEBUG) console.log('spawn called\n' + arguments);
            let result = oldSpawn.apply(this, arguments);
            return result;
        }

        child_process.spawn = mySpawn;
        let spawn = child_process.spawn;

        function onComplete(err, stdout, stderr) {
            if (err) {
                console.log('exec(' + command + '): failed.');
                console.error('err:' + err); // + '\nstderr:' + stderr);
                reject(err);
            } else {
                if (noisy) {
                    console.log('exec(' + command + '): succeeded.');
                    if (DEBUG) {
                        console.log(stdout);
                    }
                }
                resolve(stdout);
            }
        }
        //spawn the process and handle streams
        //console.log(process.env.PATH);
        //The shift() method removes the first element from an array and returns that element.
        if (DEBUG) console.log('spawn with:' + commandAndArgs);
        const proc = spawn(commandAndArgs.shift(), commandAndArgs)
            .on('error', function (err) {
                throw err;
            });
        //handle streams
        var output = '';
        proc.stdout.on('data', (data) => {
            output += data;
            if (noisy) {
                console.log('data=' + data);
            }
        });
        proc.stderr.on('data', (data) => {
            //proc.exit(1); // <<<< this works as expected and exits the process asap
            onComplete(data, '', data);
        });
        proc.on('close', (code) => {
            if (noisy) {
                console.log('child process exited with code' + code);
            }
            if (code) {
                onComplete(code, '', 'child process exited with non-zero code.');
            } else {
                onComplete(null, output);
            }
        });
        //promise won't wait forever, it will timeout
        if (timeout) {
            setTimeout(function () {
                reject('timeout:' + timeout);
            }, timeout);
        } else {
            setTimeout(function () {
                reject('timeout');
            }, 5 * 60 * 1000); //wait 5 minutes max!
        }
    });
};