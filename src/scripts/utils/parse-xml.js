// AAG 24/1/'18
// This script parses an XML string and returns it as a js object.
// (returns a promise)

module.exports = function (xml) {
    let Q = require("q");
    let parseString = require('xml2js').parseString;

    return Q.Promise(function (resolve, reject, notify) {
        parseString(xml, function (err, result) {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
};