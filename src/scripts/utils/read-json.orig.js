// AAG 23/1/'18
// This script parses a json file and returns the json object.
// (returns a promise)

module.exports = function (filename) {
    let Qfs = require("q-io/fs");
    let Q = require("q");

    let DEBUG = 0; //0=off

    if (DEBUG) console.log('read-json from file:' + filename);
    
    //when file is read, then, parse the json and return as a promise so it can be chained.
    return Qfs.read(filename).then(function (data) {
        return Q.Promise(function (resolve, reject, notify) {
            try {
                resolve(JSON.parse(data));
            } catch (err) {
                reject(err);
            }
        });
    });
};