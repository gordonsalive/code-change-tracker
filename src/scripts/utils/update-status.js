// AAG 23/1/'18
// This script updates the relevant status file for different tasks
// (returns a promise)

module.exports = function (status, type, error) {
    let Qfs = require("q-io/fs");
    let consts = require('../constants');

    let file_utils = require("./file-utils");

    let d = new Date();
    let localeDate = d.toISOString(); //toLocaleString();
    var statusFile = '';
    switch (type) {
        case 'code-changes':
            statusFile = consts.codeChangesStatus(0);
            break;
        case 'publish-files':
            statusFile = consts.publishFilesStatus(0);
            break;
        default:
            throw "Unexpected type in update-status:" + type;
    };
    let statusObj = {
        status: status,
        lastAttempt: localeDate
    };
    if (status === 'succeeded') {
        statusObj.lastSuccess = localeDate;
    }
    if (error) {
        statusObj.error = error;
    }
    //return Qfs.write(statusFile, JSON.stringify(statusObj, null, '\t'));
    return file_utils.writeFile(statusFile, JSON.stringify(statusObj, null, '\t'));
}