let lockFile = require('lockfile');
let Qfs = require("q-io/fs");
let Q = require("q");
Q.longStackSupport = true;
let fs = require("fs");

//TODO: on linux will get upset if directory does not exist.  Need to ensure directories are created as part of any write or copy.

const NO_OF_RETRIES = 5;
const RETRY_DELAY = 100;//100 ms
const C_OPTS = {
    wait: 1000,//no of ms to wait for a lock to expire before giving up
    //pollPeriod: 100,//When using opts.wait, this is the period in ms in which it polls to check if the lock has expired. Defaults to 100.
    stale: 10 * 1000,//10 seconds, how long before locks are considered to have expired
    retries: 5,//number of times to retry before giving up
    retryWait: 100//wait n ms before retryting
}

const addLock = function (filename, options) {
    const lockfile = filename + '.lock';
    console.log('addLock');
    const opts = options ? options : C_OPTS;
    return Q.Promise(function (resolve, reject, notify) {
        try {
            console.log('about to lock ' + filename + '.lock');
            if (!fs.existsSync(lockfile)) {
                fs.closeSync(fs.openSync(lockfile, 'w'));
            }
            lockFile.lock(lockfile, opts, function (er) {
                if (er) {
                    reject(er);
                } else {
                    resolve(filename);
                }
            });
        } catch (err) {
            reject(err);
        }
    });
}
module.exports.addLock = addLock;

const releaseLock = function (filename) {
    const lockfile = filename + '.lock';
    return Q.Promise(function (resolve, reject, notify) {
        try {
            lockFile.unlock(lockfile, function (er) {    
                if (er) {
                    reject(er);
                } else {
                    resolve(filename);
                }
            });
        } catch (err) {
            reject(err);
        }
    });
}
module.exports.releaseLock = releaseLock;

const addLockSync = function (filename, options) {
    const lockfile = filename + '.lock';
    const opts = options ? options : C_OPTS;
    if (!fs.existsSync(lockfile)) {
        fs.closeSync(fs.openSync(lockfile, 'w'));
    }
    return lockFile.lockSync(filename, opts);
}
module.exports.addLockSync = addLockSync;

const releaseLockSync = function (filename) {
    const lockfile = filename + '.lock';
    return lockFile.unlockSync(lockfile);
}
module.exports.releaseLockSync = releaseLockSync;

module.exports.readFileSync = function (filename) {
    //addLockSync(filename);
    try {
        const data = fs.readFileSync(filename);
    } finally {
        //releaseLockSync(filename);
    }
    return data;
}

module.exports.writeFileSync = function (filename, data) {
    //addLockSync(filename);
    try {
        const status = fs.writeFileSync(filename, data);
    } finally {
        //releaseLockSync(filename);
    }
    return status;
}

module.exports.readFile = function (filename) {
    console.log('readfile');
    // return addLock(filename)
    //     .then(Qfs.read)
    //     .then(function (data) {
    //         console.log('readfile - file read.');
    //         return releaseLock(fileName)
    //             .then(function (unlockResults) {
    //                 console.log('readfile - about to return data');
    //                 return data;
    //             });
    //     });

    return Qfs.read(filename);
}

module.exports.writeFile = function (filename, data) {
    // return addLock(filename)
    //     .then(function (filename) {
    //         return Qfs.write(filename, data);
    //     })
    //     .then(function (writeResult) {
    //         return releaseLock(fileName);
    //     });
    return Qfs.write(filename, data);
}

module.exports.copyFile = function (from, to, allowFail) {
    //TODO: add file locking (with retires)
    if (allowFail) {
        try {
            fs.createReadStream(from).pipe(fs.createWriteStream(to));
            return true;
        } catch (e) {
            console.log(e);
            return true;//gulp
        }
    } else {
        return fs.createReadStream(from).pipe(fs.createWriteStream(to));
    }
}

    // function readFile(filename) {
    //     return waitForLock()
    //         .then(function(locked) {
    //             return Qfs.read(filename)
    //         })
    //         .fin(function() {
    //             releaseLock(filename);
    //         })
    // }

    // function waitForLock(filename) {
    //     /* Whatever happens, must remember to release lock! */
    //     let lockFile = require('lockfile');
    //     let promisify = require('./promisify');

    //     function aquireLockRecursive(filename, locked = false, noOfAttempts = 10, retryWait = 100/*millis*/) {
    //         return promisify(false)//start promise chain
    //             .then(function (dummy) {
    //                 //attempt to aquire lock
    //                 return lockFile.lockSynch(filename + '.lock');
    //             })
    //             .then(function(locked){
    //                 //if not locked and we have retry attempts left then recurse
    //                 if (!locked && (noOfAttempts > 0)) {
    //                     return aquireLockRecursive(filename, locked, noOfAttempts -1, retryWait);
    //                 } else {
    //                     return locked;
    //                 }
    //             })
    //     }
    // }

    // function releaeLock(filename) {
    //     let lockFile = require('lockfile');
    //     lockFile.unlockSync(filename + '.lock');
    // }
