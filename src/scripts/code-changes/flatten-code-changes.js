// AAG 25/1/'18
// This script takes the code changes data and flattens it.
// structure coming in:
//[
// {
// 	"repo": "jhc/thenon/F63/delphi/trunk/",
// 	"changes": [
// 		{
// 			"revision": "283711",
// 			"job": null,
// 			"jira": null,
// 			"incident": null,
// 			"author": [
// 				"finnk"
// 			],
// 			"date": [
// 				"2017-10-24T13:17:43.247561Z"
// 			],
// 			"msg": [
// 				"257908  Fix conversation error when typing quickly"
// 			],
// 			"paths": [
// 				{
// 					"path": "/thenon/F63/delphi/trunk/Figaro63/Applications/Order Routing/BaseEnquiryForms/JournalEntry.pas",
// 					"kind": "file",
// 					"action": "M"
// 				},
// 				{
// 					"path": "/thenon/F63/delphi/trunk/Figaro63/Applications/Order Routing/Transactions/WithdrawalTransactions.pas",
// 					"kind": "file",
// 					"action": "M"
// 				}
// 			],
// 			"jenkinsChange": null,
// 			"autoChange": null,
// 			"pomChange": null,
// 			"noReviewChange": null,
// 			"canIgnore": null
// 		}, ...
//  want to end up with a row for each path, so each through repo, each through changes, 
//    join other arrays, each through paths adding object to flat result list

module.exports = function (codeChanges) {
    let Q = require('q');

    let DEBUG = 0; //0=off

    var results = [];

    codeChanges.forEach(repo => {
        repo.changes.forEach(change => {
            change.paths.forEach(path => {
                results.push(
                    {
                        repo: repo.repo,
                        repoName: repo.repoName,
                        branch: change.branch,
                        revision: change.revision,
                        job: change.job,
                        jira: change.jira,
                        incident: change.incident,
                        authors: change.author.join(', '),
                        dates: change.date.join(', '),
                        msgs: change.msg.join(', '),
                        path: [path.action, path.kind, path.path].join(':'),
                        jenkinsChange: (change.jenkinsChange === true),
                        autoChange: (change.autoChange === true),
                        pomChange: (change.pomChange === true),
                        noReviewChange: (change.noReviewChange === true),
                        canIgnore: (change.canIgnore === true)
                    }
                );
            });
        });
    });

    return Q(results);
};