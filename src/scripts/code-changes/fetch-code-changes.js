// AAG 23/1/'18
// This script to fetch all the SVN code changes for the last 3 months, grouped by job, jira or incident no:
// - for each of the passed in repos
// - fetch svn logs as XML for the last 3 months
// - convert to json object
// - mark up null/insignificant changes so they can be filtered out of the results 
//   (automated changes/pom file & version no changes/AUTO_NO_REVIEW/AUTO-NO-REVIEW)
// - save the results to the changes-results.json file
// (to export svn log as xml: svn log -v --xml SVNPATH > /home/admin/Desktop/XXX.xml)
// (to export svn log for a date range: svn log <url> -r {2008-09-19}:{2008-09-26} )

module.exports = function (repos) {
    console.log('fetch-code-changes');

    let DEBUG = 0; //0=off

    var Q = require("q");
    var _ = require("underscore");

    let promisify = require('../utils/promisify');
    let spawn_promise = require('../utils/spawn-promise');
    let parse_xml = require('../utils/parse-xml');

    function formatDateForSVN(aDate) {
        return '' + aDate.getFullYear() + '-' +
            ('0' + (aDate.getMonth() + 1)).slice(-2) + '-'
            + ('0' + aDate.getDate()).slice(-2);
    }

    // get today and 3 months ago - convert these into the date range of the SVN log fetch
    let today = new Date();
    let todayString = formatDateForSVN(today);
    let threeMonthsAgo = new Date()
    threeMonthsAgo.setMonth(today.getMonth() - 3);
    let threeMonthsAgoString = formatDateForSVN(threeMonthsAgo);
    let dateRangeString = '{' + threeMonthsAgoString + '}:{' + todayString + '}';

    //get the repos and pass it on
    return promisify(repos)
        .then(function (repos) {
            console.log('about to pull out all the repo details');
            //pull out all the repo details 
            var repoItems = repos.repos.delphi.concat(repos.repos.java);
            //convert it to an array of promises to fetch the logs
            return Q.all(repoItems.map(i => fetchSVNLogs(repos.svnHost, i, dateRangeString)))
                .then(function (results) {
                    //wait for the results and return them
                    //return _.flatten(results);
                    var result = _.flatten(results);
                    return result;
                });
        })
        .then(function (results) {
            console.log('about to enrich changes');
            //enrich results with job, jira or incident no, by parsing msg using regex
            return enrichChanges(results);
        });

    function enrichChanges(changes) {
        return changes.map(function(repo) {
            return {
                repo: repo.repo,
                repoName: repo.repoName,
                highRisk: 'TODO',//will come from repo.isHighRisk, configured against the repo config
                changes: repo.changes.map(function(change) {

                    let jobArray = /\b(2[56789]\d{4})\b/g.exec(change.msg);
                    let jiraArray = /\b(\w{3,5}-\d{1,4})\b/g.exec(change.msg);
                    let incidentArray = /\b(1\d{6})\b/g.exec(change.msg);
                    let isJenkinsChangeArray = /Jenkins:|JENKINS_APPROVE/g.exec(change.msg);
                    let isAutoChangeArray = /\[.+\]/g.exec(change.msg);
                    let isPomUpdateArray = /[Cc]hange parent pom|[Uu]pdate version|[Uu]pdate pom|[Rr]eplacing pom|[Cc]hange pom|[Cc]reating branch|[Uu]pdate domain version objects/g.exec(change.msg);
                    let isNoReviewArray = /AUTH-NO-REVIEW|AUTH_NO_REVIEW/g.exec(change.msg);

                    let job = (jobArray && (jobArray.length > 0)) ? jobArray[1] : null;
                    let jira = (jiraArray && (jiraArray.length > 0)) ? jiraArray[1] : null;
                    let incident = (incidentArray && (incidentArray.length > 0)) ? incidentArray[1] : null;
                    let isJenkinsChange = isJenkinsChangeArray && (isJenkinsChangeArray.length > 0);
                    let isAutoChange = isAutoChangeArray && (isAutoChangeArray.length > 0);
                    let isPomUpdate = isPomUpdateArray && (isPomUpdateArray.length > 0);
                    let isNoReview = isNoReviewArray && (isNoReviewArray.length > 0);
                    let isCanIgnore = isJenkinsChange || isAutoChange || isPomUpdate;// || isNoReview; //don't ignore, just 'cause dev didn't want it reviewed (e.g. backfit)
                    
                    return {
                        revision: change.revision,
                        branch: 'TODO',//need to parse the path to pull out the branch
                        job: job,
                        jira: jira,
                        incident: incident,
                        author: change.author,
                        date: change.date,
                        msg: change.msg,
                        paths: change.paths.map(function(path) {
                            return {
                                path: path.path,
                                kind: path.kind,
                                action: path.action
                            };
                        }),
                        jenkinsChange: isJenkinsChange,
                        autoChange: isAutoChange,
                        pomChange: isPomUpdate,
                        noReviewChange: isNoReview,
                        canIgnore: isCanIgnore
                    };
                })
            };
        });
    }

    function fetchSVNLogs(svnHost, repo, dateRange) {
        //java repos have differing standards for how they are laid out - need to handle this...
        if (repo.branches === '*') {
            return fetchSVNLog(svnHost, dateRange, repo.path, repo.name);
        } else if (repo.branches === 'trunk') {
            return fetchSVNLog(svnHost, dateRange, repo.path + "trunk/", repo.name);
        } else if (repo.branches === 'trunk+') {
            //expand the repo path - for most cases we want /trunk/ and last 5 quarterly branches: /171/, /164/, etc./
            // for some repos there are too many sub projects and they get added to over time, so just get all changes
            // even from branches we might not be intereted in.
            let now = new Date();
            let yearNow = now.getFullYear(); // e.g. 2017
            let QNow = Math.ceil((now.getMonth() + 1) / 3); // e.g. 1
            //let Q = yearNow.toString().slice(-2) + QNow; //e.g. 171
            let Q_4 = (yearNow - 1).toString().slice(-2) + '4'; //e.g. 164
            let Q_3 = QNow <= 3 ? (yearNow - 1).toString().slice(-2) + '3' : yearNow.toString().slice(-2) + '3'; //e.g. 163
            let Q_2 = QNow <= 2 ? (yearNow - 1).toString().slice(-2) + '2' : yearNow.toString().slice(-2) + '2'; //e.g. 162
            let Q_1 = QNow === 1 ? (yearNow - 1).toString().slice(-2) + '1' : yearNow.toString().slice(-2) + '1'; //e.g. 161
            return Q.all([
                fetchSVNLog(svnHost, dateRange, repo.path + "trunk/", repo.name),
                fetchSVNLog(svnHost, dateRange, repo.path + "branches/" + Q_4 + "/", repo.name),
                fetchSVNLog(svnHost, dateRange, repo.path + "branches/" + Q_3 + "/", repo.name),
                fetchSVNLog(svnHost, dateRange, repo.path + "branches/" + Q_2 + "/", repo.name),
                fetchSVNLog(svnHost, dateRange, repo.path + "branches/" + Q_1 + "/", repo.name)
            ]);
        } else {
            throw "Invalid value for branches in repo.json";
        }
    }

    function fetchSVNLog(svnHost, dateRange, repoPath, repoName) {
        //the XML will have a format like:
        //<?xml version="1.0" encoding="UTF-8"?>
        //<log>
        //  <logentry revision="285832">
        //    <author>negrod</author>
        //    <date>2017-11-01T12:07:12.388433Z</date>
        //    <paths>
        //      <path action="M" kind="file">/thenon/F63/delphi/trunk/Figaro63/Applications/Portfolio Management/Rebalance/View/Frames/CommitParametersFrame.pas</path>
        //      ...
        //    </paths>
        //    <msg>258237 - FO Portfolio Management - DefDontBulkUT and LockDontBulkUT settings inaccuracy across different Endusers (INC-1004717) </msg>
        //  </logentry>
        //  ...
        //</log>
        let exec = 'svn log -v --xml ' + svnHost + repoPath + ' -r ' + dateRange;
        if (DEBUG) console.log('exec=' + exec);

        return spawn_promise(exec, !DEBUG, 120 * 1000)
            .then(function (logXML) {
                return parse_xml(logXML);
            })
            .then(function (logJSON) {
                //tidy up the logJSON into the change data we want
                if (logJSON.log && logJSON.log.logentry) {
                    return logJSON.log.logentry.map(function (item) {
                        return {
                            revision: item['$'] ? item['$'].revision : null,
                            author: item.author,
                            date: item.date,
                            msg: item.msg,
                            paths: _.flatten(
                                item.paths.map(
                                    i => i.path.map(
                                        function (p) { 
                                            return { 
                                                path: p["_"], 
                                                kind: p["$"] ? p["$"].kind : null, 
                                                action: p["$"] ? p["$"].action : null 
                                            }; 
                                        })))
                        }
                    })
                } else {
                    return []
                }
            })
            .then(function (changes) {
                return {
                    repo: repoPath,
                    repoName: repoName,
                    changes: changes
                };
            })
            .fail(function (err) {
                console.error('spawn_promise failed:' + err);
                //if any of these calls fails, just skip over them
                return {
                    repo: repoPath,
                    repoName: repoName,
                    changes: []
                };
            });
    }
};

