console.log('================================================================================');
console.log('Start: ' + new Date().toISOString());
console.log('================================================================================');

// AAG 23/1/'18
// This script calls other scripts to:
// (1) fetch all the SVN code changes for the last 3 months, grouped by job, jira or incident no
// (2) repeat every 15 minutes between 8am and 6pm

writeOutStatus('started');

(function doMainStuff() {
    //let Qfs = require("q-io/fs");

    const file_utils = require('./utils/file-utils');
    let consts = require('./constants');
    let read_json = require('./utils/read-json');
    let fetch_code_changes = require('./code-changes/fetch-code-changes');
    let flatten_code_changes = require('./code-changes/flatten-code-changes');
    let js2xmlparser = require("js2xmlparser");

    let DEBUG = 1; //0=off

    read_json(consts.repos(0))
        .then(fetch_code_changes)
        .then(function (codeChangesResults) {
            //return Qfs.write(consts.codeChangesResults(0), JSON.stringify(codeChangesResults, null, '\t'))
            return file_utils.writeFile(consts.codeChangesResults(0), JSON.stringify(codeChangesResults, null, '\t'))
                .then(function(writeResults) {
                    //now convert and write the file out as xml
                    return file_utils.writeFile(consts.codeChangesResultsXml(0), js2xmlparser.parse("codeChanges", codeChangesResults));
                })
                .then(function (writeResult) {
                    console.log('' + new Date() + 'about to flatten results');
                    //the data has a structure, but we'd like it flat for display in a table
                    return flatten_code_changes(codeChangesResults)
                        .then(function(codeChangesResultsFlat) {
                            console.log('' + new Date() + 'about to write out code changes results flat');
                            //return Qfs.write(consts.codeChangesResultsFlat(0), JSON.stringify(codeChangesResultsFlat, null, '\t'))
                            return file_utils.writeFile(consts.codeChangesResultsFlat(0), JSON.stringify(codeChangesResultsFlat, null, '\t'))
                                .then(function(writeResults) {
                                    //now convert and write the file out as xml
                                    return file_utils.writeFile(consts.codeChangesResultsFlatXml(0), js2xmlparser.parse("codeChanges", codeChangesResultsFlat));
                                })
                        });                        
                });
        })        
        .then(function (result) {
            console.log('' + new Date() + 'about to write out success');
            writeOutStatus('succeeded');

            let d = new Date();
            if (DEBUG) console.log('Completed: ' + d.toLocaleString() + '. Wait 15 minutes before repeating...');

            setTimeout(doMainStuff, 15 * 60 * 1000); //try again in 15 minutes
        })
        .fail(function (err) {
            console.error(err);
            
            writeOutStatus('failed', err);

            let d = new Date();
            console.log(d.toLocaleString() + ': error, try again in 1 hour');

            setTimeout(doMainStuff, 60 * 60 * 1000); //try again in 1 hour
        });
})();  

function writeOutStatus(connectionStatus, error) {
    let update_status = require('./utils/update-status');

    update_status(connectionStatus, 'code-changes', error);
}