import React, { Component } from 'react';
import './css/FilterForm.css';

export class FilterForm extends Component {
	//PROPS:
	// onSubmitClick(state) - method to call when submit is called, passes in the current state object
	constructor(props) {
		super(props);
		//set up the dates
		let today = new Date();
		let tomorrow = new Date(); tomorrow.setDate(today.getDate() +1);
		let twoWeeksAgo = new Date();
		twoWeeksAgo.setDate(twoWeeksAgo.getDate() - 14);
		//let todayString = today.toLocaleDateString().split('/').join('-');
		//let twoWeeksAgoString = twoWeeksAgo.toLocaleDateString().split('/').join('-');
		let tomorrowDefaultValueString = tomorrow.toLocaleDateString().split('/').reverse().join('-');
		let twoWeeksAgoDefaultValueString = twoWeeksAgo.toLocaleDateString().split('/').reverse().join('-');

		this.state = {
			filterDates: true,
			earliestDate: twoWeeksAgoDefaultValueString,
			latestDate: tomorrowDefaultValueString//,
			//earliestDateValueString: twoWeeksAgoDefaultValueString,
			//latestDateValueString: todayDefaultValueString
		};

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInputChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name ? target.name : target.id;

		// //URGENT TODO: NEED TO CHANGE THE DATE FIELD HANDLING
		// if ((name === 'earliestDate') || (name === 'latestDate')) {
		// 	console.log('date value = ' + value);
		// 	let d = new Date(value);
		// 	let dateValueString = d.toLocaleDateString().split('/').reverse().join('-');
		// 	console.log('date value string = ' + dateValueString);
		// 	let defaultValueName = (name === 'earliestDate') ? 
		// 		'earliestDateValueString' : (name === 'latestDate') ? 'latestDateValueString' : '';
		// 	this.setState({
		// 		[name]: value,
		// 		[defaultValueName]: dateValueString
		// 	});
		// } else {
			this.setState({
				[name]: value
			});
		//}
	}

	handleSubmit(event) {
		if (this.props.onSubmitClick) {
			this.props.onSubmitClick(this.state);
		}
		event.preventDefault();//stop it going on and trying to submit the form as an HTML form
	}

	render() {
		return (
			<div className="FilterForm">
				<div className="row">
					<div className="col-xs-12 col-sm-3">
						<h3>Pre-filter results:</h3>
						<br />(will clear filters and refetch data.)
					</div>
					<div className="col-xs-12 col-sm-9">
						<form className="form-horizontal" onSubmit={this.handleSubmit}>
							<label>
								Filter Dates?
								<input
									name="filterDates"
									type="checkbox"
									checked={this.state.filterDates}
									onChange={this.handleInputChange}
								/>
							</label>
							<div className="form-group">
								<label htmlFor="earliestDate">Earliest Date (inclusive):</label>
								<input type="date" className="form-control" id="earliestDate" name="earliestDate" value={this.state.earliestDate} onChange={this.handleInputChange} />
							</div>
							<div className="form-group">
								<label htmlFor="latestDate">Latest Date (exclusive):</label>
								<input type="date" className="form-control" id="latestDate"  name="latestDate" value={this.state.latestDate} onChange={this.handleInputChange} />
							</div>
							<small>(e.g. to get changes for 15/3/18 only, put earliest date = 15/3/18 and latest date = 16/3/18)</small>
							<br />
							<button type="submit" className="btn btn-default">Submit</button>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

export default FilterForm;
