import React from 'react';
import { shallow, mount } from 'enzyme';
const snapshot = require('snap-shot');
var should = require('chai').should()
//import sinon from 'sinon';
import ResultsTable from './ResultsTable';

import Enzyme from 'enzyme';//I think these can go into a setup script
import Adapter from 'enzyme-adapter-react-16';//I think these can go into a setup script
Enzyme.configure({ adapter: new Adapter() });//I think these can go into a setup script

describe('<ResultsTable /> shallow', () => {
    it('renders a `.ResultsTable`', () => {
        const wrapper = shallow(<ResultsTable />);
        wrapper.find('.ResultsTable').should.have.length(1);
    });

    it('has`t changed form the snapshot (shallow)', () => {
        const wrapper = shallow(<ResultsTable />);
        snapshot(wrapper.text(), 'has`t changed form the snapshot (shallow)');//using snap-shot
    })
});

describe('<ResultsTable /> deep', () => {
    const columns = [
        { isKey: true, name: 'Revision', dataField: 'revision', width: '75', filter: {} },
        { name: 'Repo', dataField: 'repo', width: '200', filter: {} },
        {
            name: 'Dates', dataField: 'dates', width: '170', isDate: true,
            filter: { defaultValue: { date: new Date(2018, 0, 1), comparator: '>=' } }
        },
        { name: 'Can Ignore', dataField: 'canIgnore', width: '40', isCheckbox: true, filter: {} }
    ];
    const data = [
        {
            "repo": "jhc/thenon/F63/delphi/trunk/",
            "revision": "285257",
            "dates": "2018-01-27T15:18:22.352426Z",
            "canIgnore": false
        },
        {
            "repo": "jhc/thenon/F63/delphi/trunk/",
            "revision": "285257",
            "dates": "2018-01-27T15:18:22.352426Z",
            "canIgnore": false
        }
    ];

    it('allows us to set props', () => {
        const wrapper = mount(<ResultsTable columns={columns} fetchHandler={data} bar="baz" />);
        wrapper.props().bar.should.equal('baz');
        wrapper.setProps({ bar: 'foo' });
        wrapper.props().bar.should.equal('foo');
    });

    it('has`t changed form the snapshot (deep)', () => {
        const wrapper = mount(<ResultsTable columns={columns} fetchHandler={data} />);
        snapshot(wrapper.text(), 'has`t changed form the snapshot (deep)');//using snap-shot
    });
});