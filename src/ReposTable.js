import React, { Component } from 'react';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import './css/ReposTable.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
//const Q = require('q');
const reposData = require('./scripts/config/repos.json');

export class ReposTable extends Component {
    //PROPS:
    // none - default behaviour is that it will load and display the repos config
    render() {
        const reposDataFlat = reposData.repos.delphi.concat(reposData.repos.java);
        return (
            <div className="ReposTable" style={{height: '230px', overflow: 'scroll'}}>
                <span className={this.props.isSmall ? "small input-sm" : ""}>
                    Repos searched:
                    {/* <small> */}
                    <BootstrapTable 
                        //data={this.state.results} 
                        data={reposDataFlat} 
                        striped hover condensed>
                        <TableHeaderColumn key='name' isKey ref='name' dataField='name' width='100' dataSort={true}
                            //   filter={{ type: 'TextFilter', delay: 1000, style: {fontSize: "xx-small"} }}
                            >Name</TableHeaderColumn>
                        <TableHeaderColumn key='path' ref='path' dataField='path' width='100' dataSort={true}
                            //   filter={{ type: 'TextFilter', delay: 1000, style: {fontSize: "xx-small"} }}
                            >Path</TableHeaderColumn>
                        <TableHeaderColumn key='branches' ref='branches' dataField='branches' width='25' dataSort={true}
                            //   filter={{ type: 'TextFilter', delay: 1000, style: {fontSize: "xx-small"} }}
                            >Branch</TableHeaderColumn>
                    </BootstrapTable>
                    <small>(key: * means all directories under path; trunk means only trunk directory; trunk+ means trunk and last 5 quarterly releases, e.g.: trunk, 181, 174, 173, 172, 171)</small>
                    {/* </small> */}
                </span>
            </div>
        );
    }
}

export default ReposTable;
