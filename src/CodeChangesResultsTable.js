import React from 'react';
import './css/CodeChangesResultsTable.css';
import {ResultsTable} from './ResultsTable';

let consts = require('./scripts/constants.js');

export class CodeChangesResultsTable extends ResultsTable {
    // PROPS:
    // testData: tells component to automatically load test data
    // filterState: used when fetching data
    // onCreated: called in constructor, passing in this (to allow getData to be triggered for example)
    testData = [
        {
            "repo": "jhc/thenon/F63/delphi/trunk/",
            "repoName": "delphi",
            "branch": "F63",
            "revision": "285257",
            "job": "258880",
            "jira": null,
            "incident": null,
            "authors": "negrod",
            "dates": "2018-01-27T15:18:22.352426Z",
            "msgs": "258880 - FO CCM: Enhancements, Review Comments functionality added (BRKM3 Project, job 254614, F63 backfit REVERTED)",
            "path": "M:file:/thenon/F63/delphi/trunk/Figaro63/Applications/ConfigClientMaintenance/ApplicationController.pas",
            "jenkinsChange": false,
            "autoChange": false,
            "pomChange": false,
            "noReviewChange": false,
            "canIgnore": false
        },
        {
            "repo": "jhc/thenon/F63/delphi/trunk/",
            "repoName": "delphi",
            "branch": "F63",
            "revision": "285257",
            "job": "258880",
            "jira": null,
            "incident": null,
            "authors": "negrod",
            "dates": "2018-01-27T15:18:22.352426Z",
            "msgs": "258880 - FO CCM: Enhancements, Review Comments functionality added (BRKM3 Project, job 254614, F63 backfit REVERTED)",
            "path": "M:file:/thenon/F63/delphi/trunk/Figaro63/Applications/ConfigClientMaintenance/CCM_Main.dfm",
            "jenkinsChange": false,
            "autoChange": false,
            "pomChange": false,
            "noReviewChange": false,
            "canIgnore": false
        }
    ];
    constructor(props) {
		super(props);
		this.state = {
            //filterState: this.props.filterState filterState has been 'lifted' up out of this component
            results: [],//the data to show in the table
            isLoading: false
        };
        //(note, these binds aren't actually needed as one doesn't use 'this' and the other is called using => notation)
		this.getData = this.getData.bind(this);
        this.clearFilter = this.clearFilter.bind(this);
        //call onCreated
        if (this.props.onCreated) {
            this.props.onCreated(this);
        }
	}
    getData() {
        if (this.props.testData) {
            //return this.testData;//results has been lifted to here, so just set state
            console.log('return test data');
            this.state.results = this.testData;
        } else {
            const codeChangesResultsTable = this;
            console.log('setting isLoading to true');
            codeChangesResultsTable.setState({
                isLoading: true
            });
            // fetch the data using filterState to filter what I put into the table
            // (this is different from the table's own filter)
            //return fetch('./src/scripts/data/code-changes-results-flat.json')
            console.log('fetching data/code-changes-results-flat.json:this=');
            console.log(this);
            //let result = fetch('data/code-changes-results-flat.json')//get it from public folder?
            var fetchURL = consts.dataSources.codeChangesResultsFlat;
            if (this.props.filterState.earliestDate) {
                fetchURL = fetchURL + '?earliestDate=' + this.props.filterState.earliestDate;
            }
            console.log('fetchURL=' + fetchURL);
            let result = fetch(fetchURL)//get it from public folder?
                .then(res => {
                    //(should check the status before trying to convert to json)!!!!!!!!!!!
                    //return res.json()})
                    if (res.ok) {
                        return res.json(
                    )}
                    throw new Error('Network reponse was not OK (' + res.status + ': ' + res.statusText + ').');
                })
                .then(response => {
                    console.log('handling respose.');
                    //console.log('response=')
                    //console.log(response);
                    if (!this.props.filterState || !this.props.filterState.filterDates) {
                    //if (!this.state.filterState || !this.state.filterState.filterDates) {
                        return response.json;
                    } else {
                        console.log('going to filter the response...');
                        return response.json.filter(item => {
                            let itemDate = new Date(item.dates);
                            //if ((new Date(codeChangesResultsTable.props.filterState.earliestDate) <= new Date(itemDate)) &&
                            //(new Date(itemDate) <= new Date(codeChangesResultsTable.props.filterState.latestDate))) {
                            //    console.log('item passes filter:' + itemDate);
                            //}
                            return ((new Date(codeChangesResultsTable.props.filterState.earliestDate) <= new Date(itemDate)) &&
                                    (new Date(itemDate) <= new Date(codeChangesResultsTable.props.filterState.latestDate)));
                        });
                    }
                })
                .then(function(result) {
                    console.log('updating results:');
                    //console.log(result);
                    //results has been lifted to here, so don't return data, just set state
                    codeChangesResultsTable.setState({
                            results: result,
                            isLoading: false
                        });
                })
                .catch(error => {
                    alert('ERROR:' + error + '\n Try submitting the request again.');
                    throw error
                });
            //return this.testData;
            return result;//returns a promise!
        }
    }
    clearFilter(refs) {
        refs.revision.cleanFiltered();
        refs.repo.cleanFiltered();
        refs.repoName.cleanFiltered();
        refs.branch.cleanFiltered();
        refs.job.cleanFiltered();
        refs.jira.cleanFiltered();
        refs.incident.cleanFiltered();
        refs.authors.cleanFiltered();
        refs.dates.cleanFiltered();
        refs.msgs.cleanFiltered();
        refs.path.cleanFiltered();
        refs.canIgnore.cleanFiltered();
        refs.jenkinsChange.cleanFiltered();
        refs.autoChange.cleanFiltered();
        refs.pomChange.cleanFiltered();
        refs.noReviewChange.cleanFiltered();
    }
    render() {
        let defaultDate = new Date();
        defaultDate.setDate(defaultDate.getDate() - 14);//set default day of week to 14 days ago
        console.log('defaultDate = '+defaultDate);
        return (
            <div className="row">
                <div className="col-xs-12">
                    <div className="CodeChangesResultsTable">
                        <ResultsTable
                            results={this.state.results}
                            columns={[
                                { isKey: true, name: 'Revision', dataField: 'revision', width: '75', filter: {} },
                                { name: 'Repo', dataField: 'repo', width: '180', filter: {} },
                                { name: 'RepoName', dataField: 'repoName', width: '90', filter: {} },
                                { name: 'Branch', dataField: 'branch', width: '60', filter: {} },
                                { name: 'Job', dataField: 'job', width: '70', filter: {} },
                                { name: 'Jira', dataField: 'jira', width: '85', filter: {} },
                                { name: 'Incident', dataField: 'incident', width: '90', filter: {} },
                                { name: 'Authors', dataField: 'authors', width: '100', filter: {} },
                                { name: 'Dates', dataField: 'dates', width: '200', isDate: true, 
                                    filter: this.props.testData ? {defaultValue: { date: new Date(2018, 0, 1), comparator: '>=' }} : {defaultValue: { date: defaultDate, comparator: '>=' }} },
                                { name: 'Commit Comments', dataField: 'msgs', width: '300', filter: {} },
                                { name: 'Path', dataField: 'path', width: '300', filter: {} },
                                { name: 'Can Ignore', dataField: 'canIgnore', width: '40', isCheckbox: true, filter: {defaultValue: 'false'} },
                                { name: 'Jenkins Change', dataField: 'jenkinsChange', width: '40', isCheckbox: true, filter: {} },
                                { name: 'Automated Change', dataField: 'autoChange', width: '40', isCheckbox: true, filter: {} },
                                { name: 'POM Change', dataField: 'pomChange', width: '40', isCheckbox: true, filter: {} },
                                { name: 'No Review Change', dataField: 'noReviewChange', width: '40', isCheckbox: true, filter: {} }
                            ]}
                            fetchHandler={this.props.testData ? this.testData : () => this.getData()}
                            // clearFilterHandler={this.clearFilter.bind(this)}
                            clearFilterHandler={this.clearFilter}
                            isSmall='true'
                            isLoading={this.state.isLoading}
                            // fetchImmediate={this.props.testData ? true : false}
                        >
                        </ResultsTable>
                    </div>
                </div>
            </div>
        );
    }
}

export default CodeChangesResultsTable;
