import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

describe('App React Component', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });  
});

import { shallow, mount } from 'enzyme';
const snapshot = require('snap-shot');
var should = require('chai').should()
//import sinon from 'sinon';

import Enzyme from 'enzyme';//I think these can go into a setup script
import Adapter from 'enzyme-adapter-react-16';//I think these can go into a setup script
Enzyme.configure({ adapter: new Adapter() });//I think these can go into a setup script

describe('<App /> shallow', () => {
    it('renders a `.App`', () => {
        const wrapper = shallow(<App />);
        wrapper.find('.App').should.have.length(1);
    });

    it('has`t changed form the snapshot (shallow)', () => {
        const wrapper = shallow(<App />);
        snapshot(wrapper.text(), 'has`t changed form the snapshot (shallow)');//using snap-shot
    })
});

describe('<App /> deep', () => {
    it('allows us to set props', () => {
        const wrapper = mount(<App bar="baz" />);
        wrapper.props().bar.should.equal('baz');
        wrapper.setProps({ bar: 'foo' });
        wrapper.props().bar.should.equal('foo');
    });

    it('has`t changed form the snapshot (deep)', () => {
        const wrapper = mount(<App />);
        snapshot(wrapper.text(), 'has`t changed form the snapshot (deep)');//using snap-shot
    });
});
