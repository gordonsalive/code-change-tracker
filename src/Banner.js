import React, { Component } from 'react';
import './css/Banner.css';
import {FilterForm} from './FilterForm';
import {ReposTable} from './ReposTable';


export class Banner extends Component {
	//Banner holds the filter from and static display of config
	//PROPS:
	// onSubmitClick: method to call when submit is clicked on FilterForm, passing in FilterForm state
	render() {
		return (
			<div className="Banner">
				<div className="row">
                	<div className="col-xs-7">
						<FilterForm onSubmitClick={this.props.onSubmitClick} />
					</div>
                	<div className="col-xs-5">
						<ReposTable  />
					</div>
				</div>
			</div>
		);
	}
}

export default Banner;
