'use strict';

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
module.exports = app; // for testing

//AAG - add SwaggerUI so I can publish a read only swagger interface
var SwaggerUi = require('swagger-tools/middleware/swagger-ui');

var config = {
  appRoot: __dirname // required config
};

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // AAG - Add swagger-ui (This must be before swaggerExpress.register)
  app.use(SwaggerUi(swaggerExpress.runner.swagger));//find docs here: http://localhost:10010/docs/#/default

  // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;
  app.listen(port);

  // if (swaggerExpress.runner.swagger.paths['/hello']) {
  //   console.log('try this:\ncurl http://127.0.0.1:' + port + '/hello?name=Scott');
  // }
  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('try this:\n  curl http://127.0.0.1:' + port + '/hello?name=Scott');
  }
  console.log('for API documentaiton try this:\n  curl localhost:'+port+'/docs/ \nand in json form: \n  curl localhost:'+port+'/swagger/');
});
