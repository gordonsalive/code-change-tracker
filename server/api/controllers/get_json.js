'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');
var fs = require("fs");//TODO: use Qfs instead.
var _ = require("underscore");

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */

// **** AAG I'm going to compress it all into here, rather than have a separate file per part of the schema ***** 
module.exports = {
  repos: repos,
  publish_files_status: publish_files_status,
  code_changes_status: code_changes_status,
  code_changes_results_flat: code_changes_results_flat
};

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function repos(req, res) {
  // this sends back a JSON response 
  //res.json("" + fs.readFileSync('./data/repos.json'));
  res.json({json: JSON.parse("" + fs.readFileSync('./data/repos.json'))});
}

function publish_files_status(req, res) {
  res.json({json: JSON.parse("" + fs.readFileSync('./data/publish-files-status.json'))});
}

function code_changes_status(req, res) {
  res.json({json: JSON.parse("" + fs.readFileSync('./data/code-changes-status.json'))});
}

function code_changes_results_flat(req, res) {
  try {
    var results = JSON.parse(" " + fs.readFileSync('./data/code-changes-results-flat.json'))
  } catch (err) {
    throw new Error("Failed to parse ./data/code-changes-results-flat.json, with error: " + err + 
      "\nThe file might still be being written to.");
  }
  
  //console.log('results=')
  //console.log(results);
  //if parameters were passed in, then we need to filter the result before passing them back.
  var filterDateStr = req.swagger.params.earliestDate.value;
  var filterDate = new Date(filterDateStr);
  //console.log('filterDate=' + filterDate)
  if (filterDate) {
    results = _.filter(results, function(item) {
      var itemDate = new Date(item.dates);
      //console.log('itemDate=' + itemDate)
      return itemDate >= filterDate
    })
  }
  res.json({json: results});
}
