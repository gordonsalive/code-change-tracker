const express = require('express');
const path = require('path');
const app = express();

app.use(express.static(path.join(__dirname, 'build')));
//app.use('/static', express.static('build'));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
  //res.sendFile('build/index.html');
});

//TODO: ability to filter down large static files by:
// (1): for hierarchical data, by node path
// (2): for flat data by a simple where query
// - both XML and JSON (for XML, just take JSON and convert results to XML)

console.log('service the app on port 8085');
app.listen(8085);