#useage: make sure shell script has execute rights: chmod 777 startup.sh
#        run, might have to include directory: ./startup.sh
#nvm use
echo 'useage: ./startup.sh | npm run start-loop'
forever list
cd src/scripts
forever stop ./fetch-code-changes-loop.js
forever stop ./publish-files-loop.js
cd ../..
cd server
forever stop ./app.js
cd ..
forever stop ./serve-app.js
forever list
# rm -rf /home/radiator/.forever/*
cd src/scripts
forever start ./fetch-code-changes-loop.js
forever start ./publish-files-loop.js
cd ../..
cd server
forever start ./app.js
cd ..
forever start ./serve-app.js
forever list